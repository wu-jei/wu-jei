### 𝗪𝗘 𝗔𝗥𝗘 𝗔 𝗖𝗟𝗔𝗡. 𝗪𝗘 𝗥𝗘𝗦𝗘𝗔𝗥𝗖𝗛, 𝗗𝗘𝗩𝗘𝗟𝗢𝗣, 𝗧𝗘𝗦𝗧, 𝗙𝗜𝗫, 𝗨𝗣𝗚𝗥𝗔𝗗𝗘 𝗔𝗡𝗗 𝗞𝗘𝗘𝗣.  💖
#### 𝐖𝐎𝐑𝐋𝐃 𝐔𝐍𝐈𝐓𝐄𝐃 - TO 𝐉𝐀𝐙𝐙, TO 𝐄𝐍𝐉𝐎𝐘, TO 𝐈𝐍𝐂𝐄𝐍𝐓𝐈𝐕𝐄

## Good Links for Good Projects

### Learn the Stacks
- https://github.com/stacylondon/front-end-learning

### React
- https://github.com/Monyancha/e-learning
- https://github.com/PriontoAbdullah/e-learning-online-courses
- https://github.com/niemet0502/Tefnout
- https://github.com/brij1999/Elendil
- https://github.com/marisa2306/project-3
- https://github.com/rbhatia46/React-Portfolio

### Reac Native
- https://github.com/EduardoRodriguesF/e-learning

### Django + React
- https://github.com/vintasoftware/django-react-boilerplate
